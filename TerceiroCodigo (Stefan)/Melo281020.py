# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 08:55:46 2020

@author: admin
"""

# 28/10/2020 - Wednesday Stefan 
# Funções

############################################################

"""
Funções
- sempre returns

Procedimentos
- Altera o estado do programa sem retornar nada

Geradores
- Retorn valores diferentes à medida que são pedidos

ex: 

"""
# ## Example:
# def multiplica_numero (num, valor = 2):
#     return num * valor
# print (multiplica_numero(2))

# a = [["a",2], ["b",25], ["c",0]]
# a.sort()                           # Output: [['a', 2], ['b', 25], ['c', 0]]

# a = [["a",2], ["a",0], ["b",25], ["c",0]]
# a.sort()                             #[['a', 0], ['a', 2], ['b', 25], ['c', 0]]


############################################################
## Variável Global

# a = "c"
# def triplica (string):
#     string = string * 3
# triplica(a)
# print(a)

############################################################
# ## Range code
# def range (start, end, step=1):
#     i = start
#     while i < end:
#         yield i
#         i +-1
        
"""
Yeld
- para execuçãoe guarda estado da função

"""
############################################################

"""
Há 3 funções em python que são essenciais
- map 
--> aplica uma função a cada elemento da lista


- filter
--> filtra os elementos da lista, para os quais uma dada função retorna false

- sorted
-->

"""
# #example FILTER
# a = [1,22,33,2,16]
# def lt_dezasseis(x):
#     return x < 16  # faz um boolean para saber os numeros menores que 16

# filtered_a = list(filter(lt_dezasseis,a))
# filtered_a  # Output: [1,2]


# #example SORTED
# a = ("abc", "def", "ghi")
# sorted_a = sorted(a,key=len)
# sorted_a

# a = ("abc", "def", "ghi")
# sorted_a = sorted(a,key=len,reverse=True)
# print(sorted_a)
# ## Não aconteceu


############################################################

# ## Compressão de listas
# a = [[1,2,3],[4,5,6]]
# flattened_a = [sub_item for item in a for sub_item in item]
# print(flattened_a)
# ## [1, 2, 3, 4, 5, 6]

# for item in a:
#     for sub_item in item:
#         print(item)
# ## Output:
# #[1, 2, 3]
# #[1, 2, 3]
# #[1, 2, 3]
# #[4, 5, 6]
# #[4, 5, 6]
# #[4, 5, 6]
      
# for item in a:
#     for sub_item in item:
#         print(sub_item)
# ## Output:
# ## 1
# ## 2
# ## 3
# ## 4
# ## 5
# ## 6

############################################################

# ## EXERCICIO
# ## Considere a seguinte função:
# # x = 1
# # y = 2



# def imprimeDivisaoInteira (x,y):
#     if y == 0:
#         print("Divisão por zero")
#     else:
#         print(x//y)
    
############################################################





































































