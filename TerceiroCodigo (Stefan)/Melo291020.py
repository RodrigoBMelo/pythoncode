# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 08:42:39 2020

@author: admin
"""

# 29/10/2020 - Thursday Stefan 
# 

############################################################
""" 
Ficha 2 - Funções Simples

8) Escreva uma função (o contrato não pode ser esquecido) que elimine 
a casa das unidades de um número inteiro. 
Por exemplo, retira(537) devolve 53. Se o argumento só tiver algarismo
das unidades, a função deve devolver 0. 
Teste a função escrevendo um programa que receba um número inteiro do 
utilizador e imprima o resultado de chamada à função desenvolvida.

"""

## Stefan Answer:
    
# def retira(numero):
#     str_numero = str(numero)     #converter um  numero para string
#     if len(str_numero) > 2:
#         return 0
#     else:
#         return int(str_numero[0:-1])
    
# print(retira(537))

    ## Other answer:
# def retira(numero):
#     return numero // 10

# print(retira(537))

############################################################
""" 
Ficha 3 - Funções Simples

Funções de alto nível
1. Qual o valor de cada expressão?
a. map(lambda x:x+1, range(1,4))
b. map(lambda x:x>0, [3,−5,−2,0])
c. filter(lambda x:x>5, range(1,7))
d. filter(lambda x:x%2==0, range(1,11))
2. Determine o valor de cada uma das expressões seguintes:
a. reduce(lambda y, z: y* 3+z, range(1,5))
b. reduce (lambda x, y: x** 2+y, range(2,6))

"""

# #a)
# map(lambda x:x +1, range(1,4))  
# a = list(map(lambda x:x +1, range (1,4)))
# print(a)    # [2,3,4]
# print(list(range(1,4)))   # [1,2,3]


# #c)
# list(filter(lambda x : x>5, range(1,7)))  # [6]
# range(1,7)

"""
Neste ex acima, iamos correr uma lista de 1 a 6 porque 7 não está incluido.

"""

############################################################

## Caminho absoluto :
    ## C:\Users\admin\Desktop\pythoncode\TerceiroCodigo\blobfile.txt

## Caminho relativo : 
    ## blobfile.txt == C:\Users\admin\Desktop\pythoncode\TerceiroCodigo\blobfile.txt


# f = open("blobfile.txt", "r")
# read_result = f.read(5)
# print(read_result)





"""
Metodos de abrir um ficheiro:

"r" - Read - Default value. Opens a file for reading, error if the file does not exist
    ex: 
"a" - Append - Opens a file for appending, creates the file if it does not exist
    ex:    
"w" - Write - Opens a file for writing, creates the file if it does not exist
    ex:
"x" - Create - Creates the specified file, returns an error if the file exists
    ex:
        
        
"""

############################################################
## Python File Open

# """
# "r" - Read - Default value. Opens a file for reading, error if the file does not exist
# "a" - Append - Opens a file for appending, creates the file if it does not exist
# "w" - Write - Opens a file for writing, creates the file if it does not exist
# "x" - Create - Creates the specified file, returns an error if the file exists

# "t" - Text - Default value. Text mode
# "b" - Binary - Binary mode (e.g. images)

# """
## EXAMPLES
# f = open("demofile.txt", "r")
# print(f.read())

# f = open("demofile.txt", "r")
# print(f.read(5))


# f = open("demofile.txt", "r")
# print(f.readline())

# f = open("demofile.txt", "r")
# print(f.readline())
# print(f.readline())

# f = open("demofile.txt", "r")
# for x in f:
#   print(x)
  
# f = open("demofile.txt", "r")
# print(f.readline())
# f.close()

############################################################
## Python File Write

# """
# "a" - Append - will append to the end of the file
# "w" - Write - will overwrite any existing content

# """
## EXAMPLES

# f = open("demofile2.txt", "a")
# f.write("Now the file has more content!")
# f.close()
# #open and read the file after the appending:
# f = open("demofile2.txt", "r")
# print(f.read())


# f = open("demofile3.txt", "w")   #the "w" method will overwrite the entire file.
# f.write("Woops! I have deleted the content!")
# f.close()
# #open and read the file after the appending:
# f = open("demofile3.txt", "r")
# print(f.read())


############################################################
## Create a New File

# """

# To create a new file in Python, use the open() method, with one of the following parameters:

# "x" - Create - will create a file, returns an error if the file exist
# "a" - Append - will create a file if the specified file does not exist
# "w" - Write - will create a file if the specified file does not exist

# """

#f = open("myfile.txt", "x")  #ERROR if document allready exist

#f = open("myfile.txt", "x")  # Create a document at C:

#f = open("myfile.txt", "w")  # Create a document at C:




############################################################
# # Delete a File

# """
# To delete a file, you must import the OS module, 
# and run its os.remove() function:

# Example
# Remove the file "demofile.txt":

# """

# import os
# os.remove("demofile.txt")   # Apaga o ficheiro 



############################################################
## Check if File exist

## Example
# import os
# if os.path.exists("demofile2.txt"):  # procura o ficheiro
#   os.remove("demofile2.txt")         # apagou o ficheiro existente
# else:
#   print("The file does not exist")   # caso não exista, imprime isto

  
# ## Example
# import os
# if os.path.exists("demofile.txt"):  # procura o ficheiro
#   os.remove("demofile.txt")         # não apaga porque não existe 
# else:
#   print("The file does not exist")   # imprime isto

############################################################
## Delete Folder

# import os
# os.rmdir("test")   #Error - can only delete empty folder

############################################################







