# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 08:41:44 2020

@author: admin
"""

# 30/10/2020 - Friday Stefan 
# 

############################################################
# ## Ficha de Exercicios 4
# ## Exercicio 4)

# """
# Dados referentes a observações são frequentemente guardados em ficheiros de texto. Por
# exemplo, as temperaturas lidas a várias horas do dia, ao longo de vários dias, podem ser
# guardadas num ficheiro de números em vírgula flutuante, onde cada linha contém os valores
# das várias temperaturas medidas num dia.

# 5.6 7.8 11.7 12.6 9.3 7.3
# 6.7 8.5 11.6 11.6 9.4 7.0
# 5.4 7.2 10.5 11.1 10.0 8.3

# Utilizando a função media, escreva uma função médias que, dado o nome de um ficheiro de
# texto como o acima, imprima as temperaturas médias diárias. Deverá imprimir um valor por
# linha e tantos valores quantas as linhas do ficheiro. Sugestão: utilize o 
# método string.split(s) para obter a lista de palavras existentes numa string. 
# A função media deve apanhar a exceções referentes à utilização do ficheiro. 
# Para isso utilize um try: finally: ou um with, para se assegurar que fecha o ficheiro. 
# Exceções sobre a abertura do ficheiro deverão ser tratadas pelo chamador 
# (ver exercício seguinte).

# """

# from statistics import mean

# def medias (ficheiro):
#     try:
#         with open (ficheiro) as file_reader:
#             for line in file_reader:
#                 line = line.strip()
#                 split_line = line.strip()
#                 float_list = list(map(float,split_line))
#                 print(round(mean(float_list),2))
            
#     except:
#             print("something went wrongwhile reading the file")
  
#     finally:
#         file_reader.close()
        
# medias("temperaturas.txt")

## INCOMPLETA
   
     
###########################################################
## Ficha de Exercicios 
## Classes
## 

class Myclass:
    area = 120             # Variáveis da classe
    window_size = (2,3)
         # uma classe também tem funcões. 
         # tem que ter sempre um self (referência um objeto)
    def func(self):
       pass              # palavra reservada, e indica que há código em falta

    # variaveis e funcoes dentro de uma classe são chamados de atributos
    # tudo que esta dentro de uma classe são atributos

print(Myclass.area)           # usa notação ponto
#print(Myclass.abre_porta())   # funções só podem ser acedidas por objetos da 
                              # classe a menos que se consiga 
                              
# estas funções abaixo, chamam-se inicializadores:
#    int() 
#    float()
    



    

    













































