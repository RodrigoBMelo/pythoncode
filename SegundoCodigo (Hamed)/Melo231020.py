# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 08:34:50 2020

@author: admin
"""

# 23/10/2020 - Friday Hamed 
# Dictionary

############################################################

# ppt dictionary . slide 14, 15, 16, 17
# Dictionary

# Define Dictionary
# use {:} to define a dictionary
# Dictionary {key:value}

# d = {1:"one", 2 : "two", 3 : "three"}
# print(type(d))
# print(d)

# d1 = {1:"one", 2 : "two", 3 : "three"}
# d2 = {"1":"one", "2":"two", "3":"three"}
# print(d1)
# print(d2)

# print(d1[1])
# print(d2[1])   # ERROR in D2 the value is a string, and is not defined as 1


############################################################

# ppt dictionary . slide 14, 15, 16, 17
# Get()

# Nota: para acessar os members in tuple and list we use [index]
# get() is a way to acess members in dictionary

# Example
# d = {"1":"one", "2":"two", "3":"three"}
# x = "1" in d    # this is to check if number 1 is at d dictionary
# print(x)

# Other way

# d = {"1":"one", "2":"two", "3":"three"}
# x = "1" in d    
# y = d.get("4",False)
# print(x)
# print(y)
## INCOMPLETE


############################################################
# ppt dictionary . slide 19
# 

# d1 = {"1" : "one", "2" : "two", "3" : "three"}

# x = "one" in d1
# y = d1.get("one", False)

# for k, v in d1.itens():
#     print("key is: ", k)
#     print("value is: ", v)
    
# # ERROR

# """
# key 1 is: 1
# value 1 is: one
# key 2 is: 2
# value 2 is: two
# key 3 is: 3
# value 3 is: three


# """

############################################################
# ppt dictionary . slide 24
# Example

# """
# Input - ["x", "y", "x", "z", "y", "x"]
# Output - {'x': 3, 'y': 2, 'z': 1}

# """
# #Solução 1
# a = ["x", "y", "x", "z", "y", "x"]
# d = {}
# for i in a :                # isto é para verificar todos os itens na lista
#     if i not in d:  
#         d[i] = 1
#     else:
#         d[i] +=1
# print(d)


# #Solução 2
# a = ["x", "y", "x", "z", "y", "x"]
# d = {}
# for i in a:
#     d[i] = d.get(i,0)+1
# print(d)


# #Solução 3
# a = ["x", "y", "x", "z", "y", "x"]
# d = {}

# for i in a:
#     d[i] = d.setdefault(i,0)+1
# print(d)


############################################################
# ppt dictionary . slide 26 ?
# 

# d3 = {}
# for i in range(1,101):            # the key is i
#     d3.setdefault(i,str(i))       # my data is a key, so i transforme to a string using str()
# print(d3)


############################################################
# ppt dictionary . slide 27
# Copy()

# a = {}          # a is a empty list
# b = a           # a and b are dependent
# c = a.copy()    # a and c are independent


############################################################
# ppt dictionary . slide 30
# EXERCISE

# """
# input "abfabdcaa"
# Output {"a": 4, "b": 2, "f":1, "d":1, "c":1}

# """

# s = "abfabdcaa"
# d = {}
# for i in s:
#     d = (i) = d.get(i,0) + 1
# print(d)
#     #ERROR


############################################################
# # ppt dictionary . slide 33
# # EXERCISE

# """
# input
# line = "a dictionary is a datastructure"

# Output 
# {"a":2, "dictionary":1, "is":1, "datastructure":1}

# encontrar quantas vezes estão as palavras repetidas.
# """

# line = "a dictionary is a datastructure"
# d = {}
# s = line.split()   #split string by spaces
# print(s)           #gives me a list of words

# for i in s:
#     d[i] = d.get(i,0) + 1
# print(d)
    

############################################################
# ppt dictionary . slide 33
# EXERCISE

# """
# input
# lines = "a dictionary is a datastructure \n 
#          a set is also a datastructure"

# Output 
# {"a":4, "dictionary":1, "is":2, "datastructure":2, 
#         "set":1, "also":1}

# Objetivo: encontrar quantas vezes estão as palavras repetidas.


# #print(lines)      # a dictionary is a datastructure
#                    # a set is also a datastructure.
                   
#                    # vamos através do idex visto ter transformado em lista

# # line1 = lines.split("n")[0]
# # line1 = line1.split(".")[0]
# line1 = lines.split("\n")[0].split(".")[0]   #index 0 é a 1ª frase

# # line2 = lines.split("n")[1]
# # line2 = line2.split(".")[0]
# line2 = lines.split("\n")[1].split(".")[0]
# """

# # SOLUÇÃO

# lines = "a dictionary is a datastructure\na set is also a datastructure."

# # i = 0.1
# d = {}
# number_of_lines = len(lines.split("\n"))
# for i in range(number_of_lines):
#     line = lines.split("\n")[i].split(".")[0]
    
#     s = line.split()
#     #print(s)
    
#     for i in s:
#         d[i] = d.get(i,0) + 1
# print(d)


############################################################
## ppt dictionary . slide 39
## EXERCISE

## Calculate sum of values in dict

# Input -         d= {"a":4, "b":2, "f":1, "d":1, "c":1}
# Expected Output - 4 + 2 + 1 + 1 + 1 = 9

## Answer

# d = {"a":4, "b":2, "f":1, "d":1, "c":1}
# s = 0 
# for i in d:
#     s += d[i]

# print(s)
    
############################################################
## ppt dictionary . slide 44
##update()

# d1 = {"x":3,"y":2,"z":1}
# d2 = {"w":8,"t":7,"g":5}
# d={}
# for i in (d1,d2):
#     d.update(i)
# print(d)


############################################################
## ppt dictionary . slide 46
##Exercise

# merge 2 dictionaries together
# For same keys, sum values







