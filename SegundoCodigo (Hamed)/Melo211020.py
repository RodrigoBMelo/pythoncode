# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 08:40:43 2020

@author: admin
"""

# 21/10/2020 - Wednesday Hamed - list


############################################################
# ppt List . slide 15

# a = [5 , 7, 12]
# print(a[0])


############################################################
# ppt list . slide 17

# my_list = ["p", "r", "o", "b", "e"]
# print(my_list[-1])
# print(my_list[-5])
# print(my_list[0])

# contagem começa index - do 0 para o 4 sendo 0 na minha lista o "p"


############################################################
# ppt list . slide 18

# a =[5, 7, 12]
# # como saber o index de um caracter da lista print(a.index(caracter))

# #exexmplo
# print(a.index(7))


############################################################
# ppt list . slide 19
# compare a list whith a string

# s = "sara"
# print(s[1])
# s[1] 0 "d"  # ERROR


############################################################
# ppt list . slide 20
# list is ordered

# a = [1, 2]
# b = [2, 1]
# print(a == b)


############################################################
# ppt list . slide 21
# show itens in a list

# friends = ["Rodrigo", "Mari", "Luísa"]
# for f in friends:
#     print(f)

# for i in range(3):    #range (len(friends))
#     print(friends[i])


############################################################
# ppt list . slide 21
# show itens in a list
"""
expected Output: 
     1 2
    2 23
    23 4
    4 word
"""
    


   # option 1
# my_list = [1, 2, 23, 4, "word"]
# for i in [0, 1, 2, 3, 4]:
#     print(my_list[i], my_list[i+1])
#     if i == [3]:
#         break
    
   # option 2
# my_list = [1, 2, 23, 4, "word"]
# for i in [0, 1, 2, 3, 4]:
#     if i == len(my_list)-1 :
#         break
#     print(my_list[i], my_list[i+1])
  

   # option 3
# my_list = [1, 2, 23, 4, "word"]
# for i in range( len(my_list)-1):         
#     print(my_list[i], my_list[i+1])
    
    
############################################################
# ppt list . slide 27, 28, 29
# sicing example

# a =[7, 5, 30, 2, 6, 25]

"""
a = [start : stop]
a = [: stop]
a = [a(3:)]


"""

############################################################
# ppt list . slide 29
# slicing whith step

# a =[7, 5, 30, 2, 6, 25]
# print(a[0:7:2])
# print(a[6:0:-2])
# print(a[50:0:-2])
# print(a[ :0:-2])


############################################################
# ppt list . slide 30
# slicing & Change Values

# a =[7, 5, 30, 2, 6, 25]
# print(a[3:5]==[14, 15])
# print(a) 

    
############################################################
# ppt list . slide 31
# 

# a =[4, 7]
# b = a*2
# print(b)


############################################################
# ppt list . slide 32
# in \ not in
# a =[7, 5, 30, 2, 6, 25]
# print(14 in a)
# print(14 not in a)

###########################################################
# ppt list . slide 33
# list in a list

# a =[3, [109, 27], 4, 15]
# print(a[1])        # [109, 27]
# print(a(1))        # error
# print(a[1][1])     # 27
# print(a[1, 1])     # error
# print(len(a))      # how many numbers in the list ? 4

###########################################################
# ppt list . slide 34
# exercise

"""
find the maximum value in this list ?
find the index of maximum value also ?
calculate sum of all members whith "for" loop ?

a = [7, 5, 30, 2, 6, 25]
"""

# a = [7, 5, 30, 2, 6, 25]
# maximum_number = a[0]    #variavel que destina o numero 7
# if i in maximum_number: 
#     if i > maximum_value:
#         m=1
#         print(a)
        
        ### VER SOLUÇOES NO PPT
        
###########################################################
# ppt list . slide 38
# count insert

# a = [1, 3, 6, 5, 3]
# print(a.count(3))    # 2 vezes repetido o numero 3
# print(a.count(12))   # 0 vezes


###########################################################
# ppt list . slide 39
# remove & pop

# b = [1, 2, 6, 5, 2]
# b.remove(2)
# print(b)  # remove o primeiro numero 2

# R = [10, 15, 12, 8]
# L = R.pop(R)
# print(L)
# print(R)

  ### VER SOLUÇOES NO PPT

###########################################################
# ppt list . slide 40
# pop & Del

# y = ["a", "b", "c"]
# p = y.pop(2)        # apaga a letra pela posição do index
# print(p)
# print(y)

###########################################################
# ppt list . slide 41
# Del slicing

# r = [0, 1, 2, 3, 4, 5, 6]
# del r[2:4]    # intervalo da posição pelo index
# print (r)

###########################################################
# ppt list . slide 42
# reverse sort

# a=[1, 2, 3,]
# print(a[::-1])   #[3,2,1]
# a.reverse()
# print(a)

# b =a.reverse()  # b is none
# print(b)          #none

# a = [2,4,3,5,19]
# a.sort()
# print(a)


###########################################################
# ppt list . slide 43, 44
# extend & append

# x = [1,2,3]
# x.extend(5)     #error - input of extend must be a list
# x.extend([5])     # [...] caracteriza como list  -- (..) caracteriza como integer / int
# print(x)

# acrescentar lista a outra a list
# x = [1,2,3]
# y = [4,5]
# x.extend(y)     # X antes da função e o que junta vem depois Y
# print(x)        # [1,2,3,4,5]
# print(len(x))   # 5
# print(len(y))   # 2


# # append()
# a= [1,2,3]
# a.append(4)
# print(a)

# x = [1,2,3]
# y = [4,5]
# x.append(y)     # junta a variável Y como um index apenas, apesar de serem 2 numeros
# print(x)
# print(len(x))   # este index já tem o y acoplado então são 4
# print(len(y))   #len é a quantidade de index


###########################################################
# ppt list . slide 45
# append

# a = []    # significa que a variável A é uma lista no caso vazia
# for i in range (4):  # vai fazer loop até ao numero que eu definir
#     a.append(i)
#     print(a)
    
    
###########################################################
# ppt list . slide 47
# copy

# a = [1,2,3]
# b = a.copy()   # a,b are independet because B is only a copy
# c = a          # a, c are dependent to each other (qualquer alteração muda o outro)
# d = a[:]       # a, d are independent

#    # example:
# a[1] = 22
# c[0] = 11
# d[2] = 33

# print(a)   # [11,22,3]
# print(b)   # [1,2,3]
# print(c)   # [11,22,3]
# print(d)   # [1,2,33]

#    # example:
           ## neste caso não são uma lista, então não segue a regra acima
# x = 2    # integer não se aplica os casos de list
# y = x
# y+= 1
# print(x)    
# print(y)


###########################################################
# ppt list . slide 53
# Exercise

# a = [1,2]
# b = [1,4,5]
# c = []
# for i in a:                    # i é o primeiro valor de A
#     for j in b:                # j é o primeiro valor de B e vai atualizando pra encontrar a resposta
#         if i != j:             # significa que não é igual
#            c.append((i,j))
# print(c)                       # o Output será o loop

# answer:
    # 1st loop: i=1, j=1, c= []
    # 2nd loop: i=1, j=4, c=[(1,4)] 
    # 3rd loop: i=1, j=5, c=[(1,4), (1,5)] 
    # ... (continue, whatc ppt)

###########################################################
# ppt list . slide 55
# NaN Values

a=[2.6, float("NaN"), 4.8, 6.9, float("NaN")]
b=[]
import math
for i in a:
    if not math.isnan(i):
        b.append(i)
print(b)


###########################################################
# ppt list . slide 5
# 
                




























