# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:55:33 2020

@author: admin
"""

# 20/10/2020 - Tuesday Hamed - std Lib and Modules

############################################################

# Import Modules

# import math
# math.pi       #3.1415

###############################################################
# slide 21
# 3) From module import submodule

# Example: 
# from os import getcwd
# a = getcwd()                  #"C:\\Users\\user"
#                               # colocando a como variável, ele guarda


# 4) From module import submodule as new_name
#     Example:
# from os import getcwd as gc
# gc()           #"C:\\Users\\user"

###############################################################
# slide 24
# Exercise result

# import numpy as np
# from matplotlib import pyplot as plt
# #import matplotib.pyplot as plt
# data = np.array ([-20 ,-3 ,-2 ,-1 ,0 ,1 ,2 ,3 ,4])
# plt.boxplot( data)

###############################################################

# slide 25
 # This way is not a god ideia
 
 # from math import *
 # from module import *  # means to import everything
 
 ##############################################################
 
 # slide 26
 # not to import everything
# sum??
# from numpy import *
# sum??
 
 ##############################################################
# slide 2?


# type - gives the type os the input
# dir() - give the functions available at this input
# ??  - gives what that function does
# help - 

#Example

# a = Rodrigo
# b = 35
# c = []

###############################################################
# slide 30 ? a confirmar
# def my_func() :
#     """
#     Here i describe my func, and it does not run
    
#     """
#     print("Python")
    
    
# my_func()

###############################################################

# Slide 32 - exercise (What this functions do ?)
# Answer in next slide

#############################################################
# Slide 36

# import datetime
# now = datetime.datetime.now()
# print(now)
# print(now.year)
# print(now.month)
# print(now.day)
# print(datetime . datetime . today())

#############################################################
# # Exercise slide 38
# # My try
# import datetime
# now = datetime.datetime.now()
# print(now)
# print(now.minute)
# # """ Question: see if the minute is Even or Odd
# #     if is Even - show: Not an Odd minute
# #     if is Odd - show: Odd minute
# # """
# if now % 2 == 0 :
#     print("Not an Odd minute")
# else : # <b condition> happened
#     print("Odd minute")
    
#############################################################

# Hamed answer 1st option

# from datetime import datetime as dt
# m = dt.today().minute

# if m % 2 == 0 :
#     print("Not an Add minute")
# else:
#     print("Odd minute")

#############################################################

# Hamed answer 2nd option

# from datetime import datetime as dt
# m = dt.today().minute

# for i in range(1, 60, 2):
#     if m == i:
#         print("Odd minute")
#         break
#     else:
#         print("Not an Odd minute")
#         break
    
#############################################################
        
# Hamed answer 3rd option

# from datetime import datetime as dt
# m = dt.today().minute

# odds_list = [i for i in range(1, 60, 2)]

# if m in odds_list:
#          print("Odd minute")
# else:
#          print("Not an Odd minute")
    

#############################################################




