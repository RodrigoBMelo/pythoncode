# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 08:53:46 2020

@author: admin
"""

# 23/10/2020 - Friday Hamed 
# Dictionary

############################################################
# ppt dictionary . slide 51
# Exercise

"""
Find number of x accurences in S

Input: 
s= "Python Course"
x = ["o","r"]

Output: 
    {"o":2, "r":1}
"""
# s = "Python Course"
# x = ["o","r"]
# d = {}          
# for i in s:
#     if i in x:
#         d[i] = d.get(i, 0) +1
# print(d)


############################################################
# ppt dictionary . slide 54
# Exercise

"""
Remove duplications 
find the bug of this example ?

"""

# d={"x":3, "y":2, "z":1, "y":4, "z":2}
# r= {}

# for k,v in d.items():
#     if k not in r.keys():
#         r[k] = v
# print(r)


############################################################
# ppt dictionary . slide 57
# Exercise 8

"""
Count the numbers os "s == True"

"""

# student = [
#         {"id":123, "name":"Sophia", "s": True},
#         {"id":378, "name":"William", "s": False},
#         {"id":934, "name":"Sara", "s": True}
#         ]



#     # a = True  # is a Boolean
#     # b = False # is a Boleean
#     # we can sum Booelan and Integers # Yes
#     #     ex: False + 1 = 1
#     #         False + True + True = 2
#     #         False equal zero \ True equal 1

# # EXample
# # print(True +True)  #2
# # print(True+False)  #1
# # print(True+1)      #2
# # print(True+3.14)   #4.140000000000001

# sum_of_True = 0
# for i in student:
#     sum_of_True += i["s"]
    
# print(sum_of_True)



############################################################
# ppt dictionary . slide 61
# Example

# # Randomly choose between dictionary keys

# d = {
#      "F" : 0,
#      "B" : 0
#      }

# import random 
# for _ in range(17):    # do it 17x times
#     d[random.choice(list(d.keys()))] +=1
                     
# print(d)


############################################################

"""
a = ()       # define a empty tuple
or
a = tuple() 

b = []      # define a empty list
or
b = list()


c = {}      # define a empty dict
or
c = dict()

"""

############################################################
# ppt set . slide 17
# add / update


# f= {"a", "b", "c"}
# f.add("d")
# f.add("e","f")        #Error takes only 1 argument
# f.add(["e2","f2"])     #Error do not takes list
# f.add(("e2","f2"))     #Acepts Tuple - Output: {('e2', 'f2'), 'a', 'b', 'c', 'd'}
      
# f= {"a", "b", "c"}
# f.update("g")           #Output: {'a', 'b', 'c', 'g'}
# f.update("h","i")       #Output: {'a', 'b', 'c', 'g', 'h', 'i'}
# f.update(["h","i"])     #We can use list [] Output:{'a', 'b', 'c', 'g', 'h', 'i'}


############################################################
# ppt set . slide 18
# remove(), discard(), copy(), pop(), clear() 


# students = {"jon", "yuri", "rob"}

# #Remove()
# # Rodrigo is not in students
# students.remove("Rodrigo")   # Error

# #Discard()
# # if "Rodrigo" exist in students remove it, else No error
# students.discard("Rodrigo")


############################################################
# ppt set . slide 20
# Union, intersection & update

# X = {1,2,3}
# Y = {2,3,4}
# print(X.union(Y))
# print(X | Y)

# X = {1,2,3}
# Y = {2,3,4}
# print(X.intersection(Y))
# print(X & Y)

# X = {1,2,3}
# Y = {2,3,4}
# print(X.update(Y))
# print(X)

# # Union does not change the X


############################################################
# ppt set . slide 20
# Difference, Difference_update, Symmetryc_difference


# A = {1,2,3,4,5}
# B = {2,4,7}

# print(A-B)
# print(B-A)

# r = A.difference(B)
# print(r)
# print(A)
# print(B)

# X = {1,2,3}
# Y = {2,3,4}

# # print(X.symmetric_difference(Y))
       
# A = {1,2,3,4,5}
# B = {2,4,7} 
# r = A.Difference_update(B)
# #print(r)
# print(A)

# #Error

############################################################
# ppt set . slide 
#EXERCISE


# A = {1,2,3,4,6,9,10}
# B = {1,3,4,9,13,14,15}
# C = {1,2,3,6,9,11,12,14,15}

#Hamed answers:
# # Part 1) 
# answer_1 = C - A.intersection((B).intersection(C))  #option 1
# answer_2 = C - A.intersection(B,C)
# answer_3 = C.difference(A & B & C)

# # Part 2)
# answer2_1 = (A & B - C)
# answer2_2 = A & B - C
                                              
# # # Part 3)
# answer3_1 = A.union(B) - C
# answer3_2 = (A| B) - C

############################################################
# ppt set . slide 25
#EXERCISE - wich characters of 
# "a", "y", "c", "o", "z" are in "Python Course"?
# Output: "o", "y"

# w = "Python Course"
# char = {"a", "y", "c", "o", "z"}
# w_set = set(w)
# # INCOMPLETO ver em slide 31 ?'

############################################################
# ppt set . slide 33
# #EXERCISE - Find match key:value in 2 dictionaries
d1 = {"a":1, "b":3, "c":2}
d2 = {"a":2, "b":3, "c":1}
d1.items()
# # ANSWER: 
    



############################################################
# ppt set . slide 38
# Summary




    









































