# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 08:35:24 2020

@author: admin
"""

# 22/10/2020 - Thursday Hamed 
# Tuples

############################################################
# ppt Tuple . slide 16
# define a Tuple

# t1 = (3)    # int
# t2 = (3,)   # tuple

############################################################
# ppt Tuple . slide 17
# Acess members

# t= ("English", "History", "Mathematica")
# print(t[0])   # English

   ############################################################
# ppt Tuple . slide 17
# 

# t= ("English", "History", "Mathematica")
# #t[0] = "art"       # ERROR - Tuples are immutable like strings are
# for i in t:
#     print(f"I like to read {i}")
# if i == ("History"):
#     print("Ok")

####
# # exemplo aleatório
# a = (1,2,3)
#  for i in a:   # here nothing inside A is an assigment
#      i = a[0]  # valores a direita definem o i
#      i = a[1]
#      i = a[2]
#     ...

# if i in a: (i == a[0] or i == a[1] or i == a[2] or):
#     # INCOMPLETE

############################################################
# ppt Tuple . slide 20
# +  *  reversed

# t =(1, 9, 2)
# print(t*2)
# print(t+t+t)
# print((3,6)+(9,))
# print((1,2)+(9,6)) 

# #Reversed
# print(tuple(reversed(t)))

############################################################
# ppt Tuple . slide 21
# Ordered

# t1=(1,2)
# t2=(2,1)
# print(t1 == t2)   # False


############################################################
# ppt Tuple . slide 24
# Exercise

# Try append tuple with another way! By converting tuple to another data type

# input: t=(4,6)
# Output: t be (4,6,9)

# My answer
# t = (4,6)    # aqui é um Tuple
# print(t)
# t = [4,6]    # aqui transformei numa lista
# print(t)
# t.append(9)  # acrescentei o 9 pedido
# print(t)


# Hamed answer
# What is my input ? - It is a Tuple
# How to convert Tuple ? List() is a function
# What should i use to add/append/extend to a list ?
# How i can convert a list to Tuple ?
# Answer:
    
# t = (4,6) 
# a = list(t)    # criou variável para converter tuple em list
# a.extend([9])  # usou função extend para acrescentar o 9
# t = tuple(a)   # converteu em tuple novamente
# print(t)

############################################################
# ppt Tuple . slide 27
# Exercise

#Try change string ?
# you need to convert string to another data type 
# then overwite it whit new value!

# 1 - Remove "$", "#" and Spaces 
# 2 - 

# Objective:
# input: t = "$ $ Python$#Course $"
# Output: t = "Python Course"

# Hamed Answer:
    # Questions 
    # 1 What is my input ? --- string, unchangeable
    # 2 Remove "$", "#" and spaces?
       # is there any method to do that ? yes, t.strip("# $")
       
    # 3 - It is solved ? I need to remove "#$" from the middle of string! 
                       # ==> convert to by List()
    # 4 - i need a loop to check all list members and remove unwanted members
    # 5 - I need to insert space betweenn "n" and "C"                       

# t = "$ $ Python$#Course $"
# # a = t.strip("# $")
# a = list(t)             
      
# b = a.copy()
# for i in a:
#     if i == "#" or i == "$" or i == " " :     # i in ["#", "$"]
#        b.remove(i)
      
# C_index = b.index("C")
# b.insert(C_index, " ")    # b.insert(b.index("C"), " ")
# t="".join(b)
# print(t)

############################################################
# ppt Tuple . slide 32
# unpack

## Example 1
# t=(4,8)
# a,b=t      # atribui os valores as letras
# print(a)
# print(b)

## Example 2
# car = ("blue", "auto", 7)
# color, _, a = car
# print(color)
# print(a)
# print(_)


############################################################
# ppt Tuple . slide 33
# zip \ zip(*)

# a=(1,2)
# b=(3,4)
# c=zip(a,b)
# x=list(c)
# print(x)
# print(x[0])
# print(type(x[0]))   # type de x seria uma lista, mas o [0] segue sendo o membro

## example 1
# a=(1,2,"A")     # can use list or tuple it works
# b=(3,4,8)
# c=zip(a,b)
# x=list(c)
# print(x)

# ## minnimum length between A and RANGE(2) is 2
# a=[11,22,33,9]
# b=zip(a, range(2))
# print(list(b))

# EXERCISE
# Try Zip with 3 input variables?
# A = [1,2,"A"]
# B = ("Python", 161.8,0,5) 
# C = {10,12,14,16,18,20}

# print(list(zip(A,B,C)))   # comando List converteu os 3 para list
#                           # quando fiz zip e printei, aparecem 
#                           # ate ao numero de index da primeira variável
                          
                          
# # min(len(A), len(B), len(C)) para descobrir o valor min das 3 listas
# min_ABC = min(len(A), len(B), len(C))
# C = list(C)
# i = 0
# d1 = [(A[0], B[0], C[0]), (A[1], B[1], C[1]), (A[2], B[2], C[2])]
# d2 = [(A[i], B[i], C[i]),
#        (A[i + 1],)
#        (A[i + 2],)
    
# e = []
# COMPLETE THIS    
    
    
"""
#vamos pelo Index
[(A[0], B[0], C[0], A[1], B[1], C[1]), A[3], B[2], C[2])]

"""
############################################################
# ppt Tuple . slide 41

# EXERCISE
# How many tuples are in "num" list ?
  # Try this example whitout using "isinstance()" funtion.

Num =[8,2,(9,3),4,(1,6,7),34]     # Input é esta variável
c=0                               # inicio do loop é em zero
for i in Num:                     # loop
    if type(i) == tuple:          # no momento em que "i" for tuple é ==
        c+=1                       
print("There are ", c, "Tuples in Num")  


############################################################
# ppt Tuple . slide 44
# HOMEWORK

# Input:[(1,2,3),(4,5,6)]
# Output:[(1,2,9),()]

## Hamed explanation: search the error in slide 47 at google
## find th condition of my obkect 
## ex: search at internet what this code do

# IMPORTANT The loop funciona, precisamos é trocar a variável "my_obj = ??? "
# 


































