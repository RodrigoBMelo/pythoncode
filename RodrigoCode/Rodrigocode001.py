# # -*- coding: utf-8 -*-
# """
# Created on Thu Oct 15 15:30:15 2020

# @author: Rodrigo Melo
# """

# ##############################################################################

# # write this code from the doc Rodrigo Ventura Full Speed Python

# ##############################################################################
#   #chapter 3 page 8
# a = 2
# print(a)

# b = 2,5
# print(b)

# c = 1.5
# print(c)

# a = 2
# b = 2.5
# print(a + b)
# print((a + b) * 2)
# print (2 + 2 + 4 - 2/3)
# print(a + a + 4 - 2/3)
# b = 4
# print(a + 2 + b - 2/3)

# hi = "Hello"
# print(hi)
# bye = "Goodbye"
# print(bye)
# print(hi + "World")
# print("Hello" * 10)
# print(2 * "Hello")

# ##############################################################################
#   # exercises whith numbers page 9
#    # 1
# print(3/2)
# print(3//2)
# print(3 % 2)
# print(3**2)
# print(4/2)
# print(4//2)
# print(4 % 2)
# print(4**2)

# ##############################################################################
# # exercises whith numbers page 9
#    # 2
# numbers = [2,4]
# avg = sum(numbers)/len(numbers)
# print("The average is ", round(avg,2))

# numbers = [4,5,10]
# average = sum(numbers)/len(numbers)
# print("The average is:", round(average,6))

# numbers = [12, 14/6, 15]
# average = sum(numbers)/len(numbers)
# print("The average is:", round(average,2))

# ##############################################################################
# # exercises whith numbers page 9  
#    # 3
# pi = 3.1415
# radius = 5
# print(4/3 * pi * radius) 

# pi = 3.1415
# rad = 5
# vol = 4/3 * pi * rad
# print("The volume of the sphere is:" , vol)


# ##############################################################################
# # exercises whith numbers page 9  
#    # 4
# #  4. Use the modulo operator (%) to check which of the following 
# #   numbers is even or odd: (1, 5, 20, 60/7).
#   # Suggestion: the remainder of (x/2) is always zero when (x) is even.

# num = int(input("Enter a number: "))
# if (num % 2) == 0:
#    print("{0} is Even".format(num))
# else:
#    print("{0} is Odd".format(num))


##############################################################################
# exercises whith numbers page 9  
   # 5
#  5. Find some values for (x) and (y) such that (x < 1/3 < y) returns “True” on the
#  Python REPL. Suggestion: try (0 < 1/3 < 1) on the REPL.
   
