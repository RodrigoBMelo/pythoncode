# -*- coding: utf-8 -*-

#Author Rodrigo Melo
   
##############################################################################
  
a = 2
print(a)

b = 2,5
print(b)

c = 1.5
print(c)

a = 2
b = 2.5
print(a + b)
print((a + b) * 2)
print (2 + 2 + 4 - 2/3)
print(a + a + 4 - 2/3)
b = 4
print(a + 2 + b - 2/3)

hi = "Hello"
print(hi)
bye = "Goodbye"
print(bye)
print(hi + "World")
print("Hello" * 10)
print(2 * "Hello")

print(3/2)
print(3//2)
print(3 % 2)
print(3**2)
print(4/2)
print(4//2)
print(4 % 2)
print(4**2)

numbers = [2,4]
avg = sum(numbers)/len(numbers)
print("The average is ", round(avg,2))

numbers = [4,5,10]
average = sum(numbers)/len(numbers)
print("The average is:", round(average,6))

numbers = [12, 14/6, 15]
average = sum(numbers)/len(numbers)
print("The average is:", round(average,2))

pi = 3.1415
radius = 5
print(4/3 * pi * radius) 

pi = 3.1415
rad = 5
vol = 4/3 * pi * rad
print("The volume of the sphere is:" , vol)

############### UNTIL HERE ITS OK #########################