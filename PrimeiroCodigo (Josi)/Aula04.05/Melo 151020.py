# -*- coding: utf-8 -*-

# print("Before")
# for i in range(1000, 50000):
#     print(i)
# print("After")


  # ppt 3 slide 13 exercise 1
  
# Expected input: The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork
# Expected output: TBPHSSTCC

  # using String Methods
   # capitalize
# msg = "rodrigo"
# a= msg.capitalize()
# print(a)

#    #count
# msg = "rodrigo can not understand this"
# a= msg.capitalize()
# number_of_o = msg.count("o")
# print(number_of_o)
# print(a)

   #split
# msg = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"
# a= msg.capitalize()
# splitting_uppercase= msg.split("uppercase")
# print(splitting_uppercase)
# print(a)

#    #title
# msg = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"
# a= msg.title()
# print(a)

#    #swapcase
# msg = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"
# a= msg.swapcase()
# print(a)

#    # não aconteceu ppt 3 slide 13
# msg = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"
# if msg.isupper():
#  print(msg.isupper)

  # ppt 3 slide 15
  
# largest_so_far = -1
# print("Before", largest_so_far)
# for the_num in [9, 41, 12, 3, 74, 15]:
#     if the_num > largest_so_far :
#         largest_so_far = the_num
#     print(largest_so_far, the_num)
    
# print("After", largest_so_far)


# loop_interaction = 0
# print("Before", loop_interaction)
# for thing in [9,41, 12, 3, 74, 15] :
#     loop_interaction = loop_interaction
#     print(loop_interaction , thing)
# print("After", loop_interaction)


# print("Before")
# for value in [9,41, 12, 3, 74, 15] :
#     if value > 12:
#         print("Large number", value)
# print("After")

  # Exercicio para casa porque deu true ?
# found = False
# print("Inicio", found)
# for value in [5, 4, 3, 2, 1] :
#     if value == 3 :
#         found = True
#     print(found, value)
# print("Final",found)





