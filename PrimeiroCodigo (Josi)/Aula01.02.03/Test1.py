# -*- coding: utf-8 -*-

# print("Welcome to Python")
# print ("Welcome to Python Course")
# print(8+5*13)
# print ("Python is fun", 31)
# print ("Python is fun", 30+1)
# print ("Hello" + "Rodrigo Melo")
# print ("Hello" + "<Rodrigo Melo")
# print ("Hello" + "<Rodrigo Melo>")
# print ("Hello" + " " "Rodrigo Melo")
# print ("Hello " + "Rodrigo" + " Splitting \t spaces \n and lines")
# name = "Rodrigo"
# print (name)
# print ("Hello" , name)
# namefull = "Rodrigo Bernardo de Melo"
# print ("Hello" , name)
# print ("Hello" , namefull)
# greeting = "HEllo"
# print (greeting , namefull)
# age = "35"
# print (namefull, age)
# print (greeting * 5)
# print ("warning")
# email = "melob.rodrigo@gmail.com"
# print (email)
# print (f"{name} is {age} years old")
# print (name , age , "years old")
# print ("Please, enter your name: ")
# student_name = input()
# print ("Welcome,", student_name)
# frase = "Lisbon is in Portugal"
# print (frase)

# frase = "Lisbon is in Portugal" #23 letras com espaços e aspas
# # *********************************************
# # input:
#     # student_name
#     # student_age
# # outputs:
#     # <student_name and age>
# # This code shows the student name and age
# # *********************************************
# # print(student_name, student_age) 

#   # Abaixo faço exercicio 6 contando letras
  
# print (frase)
# print (frase [1:5])    # Queria Lisbon mas tinha que começar no 0
# print (frase [0:5])    # Queria Lisbon mas além do 0, preciso ir mais uma casa  
# print (frase [0:12])   # Queria 
# print (frase [0:16])   # Ok, Queria Lisbon is in Por

#   # Abaixo faço exercicio 7 contando letras negativas
#    # Variável é: "Lisbon is in Portugal" - são 23 letras
#    # Contagem :-0

# msg = "Lisbon is in Portugal" #23 lettters

# print(msg[-21:-15])  #Lisbon
# print(msg[-8:-5])
# print(msg[-14:-12])
# print(msg[-8:])

# print(msg[-11:-8])
# print(msg[-23:])

# print(msg[-5:-1])
# print(msg[7:])

# print(msg[:])

# # Não fiz este acima. só copiei do ppt


  # Abaixo exercicio 8
